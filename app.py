import os
import sys
import re
import json
import time
import requests
import bs4
import urllib.request
from bs4 import BeautifulSoup
from flask import Flask, render_template, send_from_directory,request


from datetime import datetime
import pypandoc

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import urllib.request
from urllib.request import urlretrieve

from sel_scrape_new import config


class crawler:
    def get_file_path_(self):
        os.environ["file_path"] = os.getcwd()
        return os.environ.get("file_path")


    def driver_initialize(self):
        driver = webdriver.Chrome()
        driver.implicitly_wait(30)
        driver.get(url)
        driver.find_element_by_id('os_username').send_keys(os.getenv('username', ''))
        driver.find_element_by_id('os_password').send_keys(os.getenv("password", ''))
        driver.find_element_by_name('login').click()
        return driver


    def initialize_parsing(self):
        driver = self.driver_initialize()
        src = driver.page_source
        soup = BeautifulSoup('src', 'lxml')
        main_title_rep = driver.title
        if "- CSD" in main_title_rep:
            main_title_rep=main_title_rep.replace("- CSD","")
        main_title=main_title_rep.replace("- Qwiki","")
        print("main title: ", main_title)
        main_title_split=main_title.replace("-","").split()
        main_title_new='-'.join(main_title_split)

        os.makedirs(main_title_new)
        self.json_file_creation(json,driver)
        self.generate_link_list(driver, main_title_new,main_title)


    def json_file_creation(self,json, driver):
        data = []

        fname = self.get_file_path_() + '/index.json'

        entry = {domain.group(0): {"url": url,
                                   "time": str(datetime.now())}}


        if not os.path.exists(fname):
            data.append(entry)
            with open(fname, mode='w') as f:
                f.write(json.dumps(data))
        else:
            with open(fname) as outfile:
                feeds = json.load(outfile)
                feeds.append(entry)
            with open(fname, mode='w') as f:
                f.write(json.dumps(feeds))



    def generate_link_list(self,driver, main_title_new,main_title):
        webelement_front = driver.find_element_by_xpath("//*[@id='splitter-content']")
        #webelement_front=driver.find_element_by_class_name("article")
        #  for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
        links = webelement_front.find_elements_by_tag_name('a')
        linklist = []  ###to store the links with same domain name as main url
        order = 0
        self.index = 0
        for link in links:
            link_href = str(link.get_attribute("href"))
            if re.findall(str(domain.group(0)), link_href) and link_href not in linklist:
                linklist.append(link_href)

        for i in linklist:
            driver.get(i)
            time.sleep(5)
            order = order + 1
            self.data_collection(driver,order, main_title_new,linklist,main_title)
            self.child_index=0
            try:
                sublink = driver.find_element_by_xpath("//*[@id='page-children']")
                sublinks = sublink.find_elements_by_tag_name('a')
                sublist = []
                for sub in sublinks:
                    sub_href = str(sub.get_attribute("href"))
                    if re.findall(str(domain.group(0)), sub_href) and sub_href not in sublist:
                        sublist.append(sub_href)

                with open("child_pages.txt", 'a') as f:
                    f.write(main_title + "\n")
                    print("writing child pages")
                    for s in sublist:
                        f.write(s + '\n')
                # for child in sublist:
                #     print("in child loop")
                #     print(child)
                #     driver.get(child)
                #     time.sleep(5)
                    #self.child_datacollection(driver,main_title_new)
                    #driver.back()

            except:
                print("no child page")

    def data_collection(self,driver, order, main_title_new,linklist,main_title):
        title_rep = str(driver.title).replace('- CSD',"")
        title=title_rep.replace('- Qwiki',"")
        print(title)
        title_list=title.replace("-","").split()
        self.title_new='-'.join(title_list)
        page_source = driver.page_source
        img_src = []
        images = driver.find_elements_by_tag_name('img')
        for image in images:
            img_src.append(image.get_attribute("src"))

        self.copying_to_json(order, title, main_title)
        self.saving_image_src(img_src,title,main_title)
        self.converting_pagesource_md(page_source, main_title_new,title,linklist)

    def child_datacollection(self,driver,main_title_new):
        child_title = str(driver.title)
        title_list = child_title.replace("-", "").split()
        child_title_new = '-'.join(title_list)
        child_page_source=driver.page_source
        self.child_converting_to_pagesource(child_page_source, main_title_new,child_title_new,child_title)



    def copying_to_json(self,order, title, main_title):
        record = []
        filename = self.get_file_path_() + '/' + main_title + '/index.json'
        new_entry = {'order': str(order),
                     'title': title,
                     "time": str(datetime.now())
                     }
        if not os.path.exists(filename):
            record.append(new_entry)
            with open(filename, mode='w') as f:
                f.write(json.dumps(record))
        else:
            with open(filename) as fp:
                datas = json.load(fp)
                datas.append(new_entry)
            with open(filename, mode='w') as f:
                f.write(json.dumps(datas))


    def saving_image_src(self,img_src, title, main_title):
        img = []
        img_src_filepath = self.get_file_path_() + '/' + main_title + "/assets"
        if not os.path.exists(img_src_filepath):
            os.mkdir(img_src_filepath)
        img_file_path = img_src_filepath + "/image.json"
        images = {'page_title': title,
                  'image_src': str(img_src)
                  }
        if not os.path.exists(img_file_path):
            img.append(images)
            with open(img_file_path, mode='w') as f:
                f.write(json.dumps(img))
        else:
            with open(img_file_path) as fp:
                ims = json.load(fp)
                ims.append(images)
            with open(img_file_path, mode='w') as f:
                f.write(json.dumps(ims))


    def converting_pagesource_md(self,page_source, main_title_new,title,linklist):
        sample_filepath = self.get_file_path_() + '/' + main_title_new + "/" + self.title_new + "_sample.md"
        filepath = self.get_file_path_() + '/' + main_title_new + "/" + self.title_new + ".html"
        file_folder = self.get_file_path_() + '/' + main_title_new + "/"+ main_title_new
        md_filepath=file_folder+"/"+self.title_new + ".md"
        if os.path.exists(md_filepath):
            return
        if not os.path.exists(file_folder):
            os.mkdir(file_folder)

        with open(filepath, 'wb')as fp:
            fp.write(page_source.encode("utf-8"))
        page_md = pypandoc.convert_text(page_source, 'md', format='html')
        with open(sample_filepath, 'w', encoding="utf-8") as fp:
            fp.write(page_md)
        flag = False
        with open(sample_filepath, 'r') as filep:
             for line in filep:
                 if "::: {.wiki-content}" in line:
                     self.headers_to_add(main_title_new, title, md_filepath, linklist)
                     break

        with open(sample_filepath, 'r') as out:
                for line in out:
                    if "#title-text" in line and "::: {.wiki-content}" in line:
                        with open(md_filepath, 'a',encoding="utf-8") as outfile:
                            outfile.write(line)
                    if "Â" in line:
                        line=line.replace("Â", '')
                    if 'style=' in line:
                        style=re.search('style(.+?);', line)
                        if style:
                            res=style.group(0)
                            line=line.replace(res,"")
                    if flag or "{.wiki-content}" in line:  # comment for for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
                        if "::: {.wiki-content}" in line or "::: {#likes-and-labels-container}" in line or not ':::' in line: #comment for for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
                    #     if not ':::' in line:     ##use for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
                            if not any (elem in line for elem in ["Â","<!-- -->","\{","\}","</div>","<div>","\{.aui-icon","null",".plugin_pagetree_children_span",".icon","null"]):
                                if "::: {#likes-and-labels-container}" in line:
                                   break
                                if len(line.strip()) != 0:
                                    if not "::: {.wiki-content}" in line:
                                       with open(md_filepath, 'a', encoding="utf-8") as outfile:
                                            outfile.write(line)
                                flag = True
                        self.md_validaton(md_filepath)


    def headers_to_add(self,main_title_new, title, md_filepath, linklist):
        self.index = self.index + 1
        #if len(linklist)<=1:

        with open(md_filepath, 'a', encoding="utf-8") as out:
            path = "/" + main_title_new + "/" + self.title_new
            out.write("---" + "\n"
                      "site: " "static"+"\n"
                      "path: "+ "/static" + path + "\n"
                      "title: " + " " + title + "\n"
                      "menuTitle: " + " " + title + "\n"
                      "draft: false" + "\n"
                      "weight: " + str(self.index)  + "\n"
                      "---" + "\n")
        # else:
        #     with open(md_filepath, 'a', encoding="utf-8") as out:
        #         path = "/" + main_title_new + "/" + self.title_new
        #         out.write("---" + "\n"
        #                   "path: " + path + "\n"
        #                   "title:" + " " + title + "\n"
        #                   "menuTitle:" + " " + title + "\n"
        #                   "draft: false" + "\n"
        #                   "weight:" + str(self.index) + "\n"
        #                   "---" + "\n")

    def child_converting_to_pagesource(self,child_page_source, main_title_new,child_title_new,child_title):
        print("in child_converting_to_pagesource")
        sample_filepath_child = self.get_file_path_() + '/' + main_title_new + "/" + child_title_new + "_sample.md"
        filepath_child = self.get_file_path_() + '/' + main_title_new + "/" + child_title_new + ".html"
        file_folder_child = self.get_file_path_() + '/' + main_title_new + "/" + main_title_new+"/"+ self.title_new
        md_filepath_child = file_folder_child + "/" + child_title_new + ".md"
        if os.path.exists(md_filepath_child):
            return
        if not os.path.exists(file_folder_child):
            os.makedirs(file_folder_child)
            print("made file path")
        with open(filepath_child, 'wb')as fp:
            print("got html")
            fp.write(child_page_source.encode("utf-8"))
        child_page_md = pypandoc.convert_text(child_page_source, 'md', format='html')
        with open(sample_filepath_child, 'w', encoding="utf-8") as fp:
            fp.write(child_page_md)
            print("got child page src")
        flag = False
        with open(sample_filepath_child, 'r') as filep:
                for line in filep:
                    if "::: {.wiki-content}" in line:
                        print("found wiki")
                        self.child_headers_to_add(child_title_new, main_title_new, child_title, md_filepath_child)
                        break
        with open(sample_filepath_child, 'r') as out:
            for line in out:
                if "#title-text" in line and "::: {.wiki-content}" in line:
                    with open(md_filepath_child, 'a', encoding="utf-8") as outfile:
                        outfile.write(line)
                if "Â" in line:
                    line = line.replace("Â", '')
                if 'style=' in line:
                    style = re.search('style(.+?);', line)
                    if style:
                        res = style.group(0)
                        line = line.replace(res, "")
                if flag or "{.wiki-content}" in line:  # comment for for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
                    if "::: {.wiki-content}" in line or "::: {#likes-and-labels-container}" in line or not ':::' in line:  # comment for for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
                        # if not ':::' in line:     ##use for link https://qwiki.internal.t-mobile.com/download/attachments/125656903/overview.html
                        if not any(elem in line for elem in
                                   ["Â", "<!-- -->", "\{", "\}", "</div>", "<div>", "\{.aui-icon", "null",
                                    ".plugin_pagetree_children_span", ".icon", "null"]):
                            if "::: {#likes-and-labels-container}" in line:
                                break
                            if len(line.strip()) != 0:
                                if not "::: {.wiki-content}" in line:
                                    with open(md_filepath_child, 'a', encoding="utf-8") as outfile:
                                        outfile.write(line)
                            flag = True
        self.md_validaton(md_filepath_child)



    def child_headers_to_add(self,child_title_new, main_title_new, child_title, md_filepath_child):
        self.child_index=self.child_index+1
        print("in child--length is ",self.check)
        with open(md_filepath_child, 'a', encoding="utf-8") as out:
            path = "/" + main_title_new + "/" + self.title_new + "/"+ child_title_new
            out.write("---" + "\n"
                      "site: " + "static" + "\n"
                      "path: " + "/static"+ path + "\n"
                      "title: " +  " "+child_title + "\n"
                      "menuTitle: " +" "+ child_title + "\n"
                      "draft: false" + "\n"
                      "weight: " + str(self.index) + "." + str(self.child_index)+ "\n"
                      "---" + "\n")


    def md_validaton(self,md_filepath):
        print("in md validation")
        with open(md_filepath, 'r+')as rp:
            value = False
            lines = rp.readlines()
            first_line = (lines[0].strip())
            print(first_line)
            if first_line != "---":
                print("halo")
                first_line = "---" + "\n"
                rp.seek(0, 0)
                rp.write(first_line)

        with open(md_filepath, 'r+')as rrp:
            content = [line for line in rrp]
            print(content)
            print("going to be empty")
            rrp.truncate(0)

        for line in content:
            if value or any(elem in line for elem in ["---", 'path', 'title', 'menuTitle', 'draft', 'weight', '---']):
                if "path" in line:
                    if "  " in line:
                        print("more space")
                        line.replace("  ", "-")
                    if ("- CSD" in line):
                        line.replace("- CSD", "")
                    if "- Qwiki" in line:
                        line.replace("- Qwiki", "")
                if "title" in line:
                    if re.search(r'\d+', line):
                        line = ''.join([i for i in line if not i.isdigit()])
                if "menuTitle" in line:
                    if re.search(r'\d+', line):
                        line = ''.join([i for i in line if not i.isdigit()])
                if "weight" in line:
                    if any(c.isalpha() for c in line):
                        ptr = re.search(r':(.*)', line, re.DOTALL)
                        ptr_dom = (ptr.group(0))
                        line = "weight: " + re.sub("[^0-9]", "", ptr_dom) + "\n"
            if len(line.strip()) != 0:
                with open(md_filepath, 'a')as wp:
                    wp.write(line)
                print("finished")



if __name__ == "__main__":
    url = sys.argv[1]
    domain = re.search('//(.+?)/', url)
    dom_string = str(domain.group(0))
    cr = crawler()
    cr.initialize_parsing()























