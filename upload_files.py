import os
import sys
import re
import json
import time
import pytablereader as ptr
import pytablewriter as ptw
import xlrd
import pptx2md
import PyPDF2
from PyPDF2 import PdfFileReader
from pptx import Presentation


from flask import Flask, render_template, send_from_directory,request

import pypandoc



# function to upload a document of any type like docx,org etc(which is supported in pypandoc.get_pandoc_formats() and convert it to markdown

def file_upload():

    app = Flask(__name__)

    APP_ROOT = os.path.dirname(os.path.abspath(__file__))


    @app.route("/")
    def index():
        return render_template("upload.html")


    @app.route("/upload", methods=['POST'])
    def upload():
        target = os.path.join(APP_ROOT, "uploaded_file/")
        if not os.path.isdir(target):
            os.mkdir(target)
        for file in request.files.getlist("file"):
            filename = file.filename
            file_split = filename.replace("-", "").split()
            filename_new = '-'.join(file_split)
            destination = "/".join([target, filename_new])
            file.save(destination)
            file_name = target + '/' + filename_new
            print(filename_new)
            extension=filename_new.split(".")[1]
            with open("ext.txt","w")as wp:
                wp.write(extension)
            item_name = filename_new.split('.')[0]
            item_name_old = filename.split('.')[0]
            print(item_name)
            print(item_name_old)
            file_folder = target + "/" + item_name
            if not os.path.exists(file_folder):
                os.mkdir(file_folder)
            if extension=="xlsx":
                md_path = file_folder + "/" + item_name + ".md"
                file_md = ptr.ExcelTableFileLoader(file_name)
                for table_data in file_md.load():
                    with open(md_path, 'a')as out:
                        headers_to_add_upload(md_path, item_name, item_name_old, file_folder)
                        out.write((format(ptw.dump_tabledata(table_data))))
                break

            if extension=="pptx":
                md_path = file_folder + "/" + item_name + ".md"
                text_file=file_name+".textile"
                prs = Presentation(file_name)
                for slide in prs.slides:
                    for shape in slide.shapes:
                        if hasattr(shape, "text"):
                            ppt_text = (shape.text)

                            with open(text_file, 'a')as pp:
                                pp.write(ppt_text)
                file_md = pypandoc.convert_file(text_file, 'md')

                with open(md_path, 'a', encoding='utf-8')as rrrp:
                    headers_to_add_upload(md_path, item_name, item_name_old, file_folder)
                    rrrp.write(file_md)

            if extension=="pdf":
                with open("just.txt",'w')as fff:
                    fff.write("got pdf")
                md_path = file_folder + "/" + item_name + ".md"
                text_file=file_name+".textile"
                text = ""

                pdf_file = open(file_name, 'rb')
                text = ""
                read_pdf = PyPDF2.PdfFileReader(pdf_file)
                c = read_pdf.numPages
                for i in range(c):
                    page = read_pdf.getPage(i)
                    text += (page.extractText()) + "\n"
                with open(text_file, 'w', encoding='utf-8')as wp:
                    wp.write(text)
                file_md = pypandoc.convert_file(text_file, 'md')
                with open(md_path, 'a', encoding='utf-8')as fpp:
                    headers_to_add_upload(md_path, item_name, item_name_old, file_folder)
                    fpp.write(file_md)
            else:
                file_md = pypandoc.convert_file(file_name, 'md')
                md_path_sample = target + "/" + item_name + "_sample.md"
                md_path = file_folder + "/" + item_name + ".md"
                with open(md_path_sample, 'w', encoding="utf-8")as out:
                    out.write(file_md)
                with open(md_path_sample, 'r', encoding="utf-8")as outfile:
                    headers_to_add_upload(md_path, item_name, item_name_old, file_folder)
                    for line in outfile:
                        if ":::" in line:
                            line = line.replace(":::", '')
                        if 'style=' in line:
                            style = re.search('style(.+?);', line)
                            if style:
                                res = style.group(0)
                                line = line.replace(res, "")
                        if not any(elem in line for elem in
                                   ["Â", "<!-- -->", "\{", "\}", "</div>", "<div>", "\{.aui-icon",
                                    ".plugin_pagetree_children_span", ".icon", "null"]):
                            if len(line.strip()) != 0:
                                with open(md_path, 'a', encoding="utf-8")as fw:
                                    fw.write(line)

        return render_template("complete.html")


    def headers_to_add_upload(md_path, item_name, item_name_old, file_folder):
        check = len(os.listdir(file_folder)) + 1
        if check <= 1:
            with open(md_path, 'a', encoding="utf-8") as out:
                path = "/" + item_name + "/" + item_name
                out.write("---" + "\n"
                          "path: " "/static" + path + "\n"
                          "title:" + str(check) + " " + item_name_old + "\n"
                          "menuTitle:" + str(check) + " " + item_name_old + "\n"
                          "draft: false" + "\n"
                          "weight:" + str(check) + "\n"
                          "---" + "\n")
        else:
            with open(md_path, 'a', encoding="utf-8") as out:
                path = "/" + item_name + "/" + item_name
                out.write("---" + "\n"
                          "path: " + path + "\n"
                          "title:" + str(check) + " " + item_name_old + "\n"
                           "menuTitle:" + str(check) + " " + item_name_old + "\n"
                           "draft: false" + "\n"
                           "weight:" + str(check) + "\n"
                           "---" + "\n")


    app.run(port=8091, debug=True)


if __name__=="__main__":
   file_upload()







