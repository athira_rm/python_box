# Python_box
# to install the dependencies use command: 
  pip install -r requirements.txt

rename the file config_sample.py as 'config.py' and pass the credentials to it

#upload_file.py

-to upload a file of any type(docx,pptx,xlsx,pdf etc) and use convert to md use: upload_file.py

-it supports files of extension- commonmark, creole, docbook, docx, epub, fb2, gfm, haddock, html, jats, json, latex, man, markdown, markdown_github, markdown_mmd, markdown_phpextra, markdown_strict,
mediawiki, muse, native, odt, opml, org, rst, t2t, textile, tikiwiki, twiki, vimwiki,pptx,xlsx,pdf

#app.py
-to convert the "Qwiki" pages to md, use: app.py

